
<h1>
  <a href="#" class="fa-solid fa-money-bill-transfer"></a>
  Cajeros
</h1>
<div class="row">
  <div class="col-md-12 text-end">
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
      <i class="fa fa-eye"></i> Ver Mapa
    </button>

    <a href="<?php echo site_url('cajeros/nuevo'); ?>" class="btn btn-outline-success">
      <i class="fa fa-plus-circle fa-1x"></i>
      Agregar Cajero
    </a>

  </div>
</div>
<br>
<br>

<?php if ($listadoCajeros): ?>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>CIUDAD</th>
        <th>LATITUD</th>
         <th>LOGITUD</th>
         <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoCajeros as $cajero): ?>
        <tr>
          <td> <?php echo $cajero->idcaj; ?></td>
          <td> <?php echo $cajero->ciudadcaj; ?></td>
          <td> <?php echo $cajero->latitud; ?></td>
          <td> <?php echo $cajero->longitud; ?></td>
          <td>
            <a href="<?php echo site_url('cajeros/editar/').$cajero->idcaj; ?>"
                 class="btn btn-warning"
                 title="Editar">
              <i class="fa fa-pen"></i>
            </a>
            <a href="<?php echo site_url('cajeros/borrar/').$cajero->idcaj; ?>"
              class="btn btn-danger"
              title="Eliminar">
           <i class="fa fa-trash"></i>
              </a>
          </td>
        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>

  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <i class="fa fa-eye"></i> Mapa de Cajeros
          </h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div id="reporteMapa" style="height:300px; width:100%; border:2px solid black;">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-bs-dismiss="modal"> <i class="fa fa-times"></i> Cerrar</button>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
      function initMap(){
        var coordenadaCentral=
            new google.maps.LatLng(-0.152948869329262,
              -78.4868431364856);
        var miMapa=new google.maps.Map(
          document.getElementById('reporteMapa'),
          {
            center:coordenadaCentral,
            zoom:8,
            mapTypeId:google.maps.MapTypeId.ROADMAP
          }
        );
        <?php foreach ($listadoCajeros as $cajero): ?>
        var coordenadaTemporal=
            new google.maps.LatLng(
              <?php echo $cajero->latitud; ?>,
              <?php echo $cajero->longitud; ?>);
          var marcador=new google.maps.Marker({
            position:coordenadaTemporal,
            map:miMapa,
            title:'<?php echo $cajero->ciudadcaj; ?>',
          });
        <?php endforeach; ?>

      }
    </script>

<?php else: ?>
  <div class="alert alert-danger">
    No se encontro cajeros registrados
  </div>

<?php endif; ?>

<br>
<br>
