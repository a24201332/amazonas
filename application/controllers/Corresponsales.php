<?php
  class Corresponsales extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model("Corresponsal");
    }

    // Renderización de la vista index de corresponsales
    public function index(){
      $data["listadoCorresponsales"] = $this->Corresponsal->consultarTodos();
      $this->load->view("header");
      $this->load->view("corresponsales/index", $data);
      $this->load->view("footer");
    }

    // Eliminación de corresponsales recibiendo el id por GET
    public function borrar($idcor){
      $this->Corresponsal->eliminar($idcor);
      $this->session->set_flashdata("eliminacion", "Corresponsal eliminado exitosamente");
      redirect("corresponsales/index");
    }

    // Renderización del formulario de nuevo corresponsal
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("corresponsales/nuevo");
      $this->load->view("footer");
    }

    // Capturando datos e insertando en corresponsal
    public function guardarCorresponsal(){
      $datosNuevoCorresponsal = array(
        "nombrecor" => $this->input->post("nombrecor"),
        "fechacor" => $this->input->post("fechacor"),
        "propietario" => $this->input->post("propietario"),
        "ciudad" => $this->input->post("ciudad"),
        "latitud" => $this->input->post("latitud"),
        "longitud" => $this->input->post("longitud"),
      );
      $this->Corresponsal->insertar($datosNuevoCorresponsal);
      $this->session->set_flashdata("confirmacion", "Corresponsal guardado exitosamente");
      enviarEmail("jose.iza9413@utc.edu.ec", "CREACION", "<h1>SE CREO EL Corresponsal </h1>" . $datosNuevoCorresponsal['nombrecor']);
      redirect('corresponsales/index');
    }

    // Renderizar el formulario de edición
    public function editar($id){
      $data["corresponsalEditar"] = $this->Corresponsal->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("corresponsales/editar", $data);
      $this->load->view("footer");
    }

    public function actualizarCorresponsal(){
     $idcor = $this->input->post("idcor");
     $datosCorresponsal = array(
       "nombrecor" => $this->input->post("nombrecor"),
       "fechacor" => $this->input->post("fechacor"),
       "propietario" => $this->input->post("propietario"),
       "ciudad" => $this->input->post("ciudad"),
       "latitud" => $this->input->post("latitud"),
       "longitud" => $this->input->post("longitud")
     );
     $this->Corresponsal->actualizar($idcor, $datosCorresponsal);
     $this->session->set_flashdata("confirmacion", "Corresponsal actualizado exitosamente");
     redirect('corresponsales/index');
   }
  }
?>
