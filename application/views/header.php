<!DOCTYPE html>
<html lang="es" dir="ltr">
	<head>
		<meta charset="utf-8">
		<title> Banco Amazonas </title>
    <!-- Importacion de Bootstrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
		<!-- Importacion de fontawesome para iconos -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
		<!-- Importacion de sweetalert -->
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.2/dist/sweetalert2.all.min.js"></script>
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.2/dist/sweetalert2.min.css">


		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCmOS7zW2jPIMscWp2_Dl9NP0g-flVRerk&callback=initMap" defer></script>

		<style>
    /* Estilos personalizados */
    body {
      background-color: #f00a07;
      color: #343a40;
    }
    .container {
      background-color: #b6b6b6; /* Color de fondo */
      padding: 20px;
      border-radius: 10px;
      box-shadow: 0 0 10px rgba(0,0,0,0.1);
    }
    h1 {
      color: #000000;
      text-align: center;
      margin-top: 20px;
    }
    .btn-outline-primary {
      color: #007bff;
      border-color: #007bff;
    }
    .btn-outline-primary:hover {
      background-color: #007bff;
      color: #fff;
    }
    .btn-outline-success {
      color: #28a745;
      border-color: #28a745;
    }
    .btn-outline-success:hover {
      background-color: #28a745;
      color: #fff;
    }
    .table {
      margin-top: 20px;
    }
    .modal-title {
      color: #007bff;
    }
  </style>
	</head>
	<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="<?php echo site_url(); ?>">Banco Amazonas</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="<?php echo site_url(); ?>">Inicio</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('bancos/index') ?>">Bancos</a>
        </li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo site_url('agencias/index') ?>">Agencias</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo site_url('cajeros/index') ?>">Cajeros</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo site_url('corresponsales/index') ?>">Corresponsales</a>

    </div>
  </div>
</nav>
<div class="container">
	<!--

	-->
<?php if ($this->session->flashdata("confirmacion")): ?>
	<script type="text/javascript">
			Swal.fire({
  			title: "CONFIRMACION",
  			text: "<?php echo $this->session->flashdata('confirmacion'); ?>",
  			icon: "success" //error, warning, info, succes
			});
		</script>
		<?php $this->session->set_flashdata('confirmacion','') ?>
<?php endif; ?>

<?php if ($this->session->flashdata("eliminacion")): ?>
	<script type="text/javascript">
			Swal.fire({
  			title: "ELIMINACION",
  			text: "<?php echo $this->session->flashdata('eliminacion'); ?>",
  			icon: "error" //error, warning, info, succes
			});
		</script>
		<?php $this->session->set_flashdata('eliminacion','') ?>
<?php endif; ?>
