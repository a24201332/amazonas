<?php
  class Cajeros extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model("Cajero");
    }

    //Renderizacion de la vista index de hospitales
    public function index(){
      $data["listadoCajeros"]=$this->Cajero->consultarTodos();
      $this->load->view("header");
      $this->load->view("cajeros/index",$data);
      $this->load->view("footer");

    }
    //Eliminacion de hospitales recibiendo el id por get
    public function borrar($idban){
      $this->Cajero->eliminar($idcaj);
      $this->session->set_flashdata("eliminacion","Cajero eliminado exitosamente");
      redirect("cajeros/index");

    }
    //Renderizacion del formulario de nuevo hospital
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("cajeros/nuevo");
      $this->load->view("footer");
    }

    //Capturando datos e insertando en Hospital
    public function guardarCajeros(){
      $datosNuevoCajero=array(
        "ciudadcaj"=>$this->input->post("ciudadcaj"),
        "latitud"=>$this->input->post("latitud"),
        "longitud"=>$this->input->post("longitud"),
      );
      $this->Cajero->insertar($datosNuevoCajero);
      $this->session->set_flashdata("confirmacion","Cajero guardado exitosamente");
      enviarEmail("jose.iza9413@utc.edu.ec","CREACION",
         "<h1>SE CREO EL Cajero de la ciudad </h1>".$datosNuevoCajero['ciudadcaj']);
      redirect('cajeros/index');
    }

    //Renderizar el formulario de edicion
    public function editar($id){
      $data["cajeroEditar"]=$this->Cajero->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("cajeros/editar",$data);
      $this->load->view("footer");
    }
    public function actualizarCajero(){
     $idcaj=$this->input->post("idcaj");
     $datosCajero=array(
       "ciudadcaj"=>$this->input->post("ciudadcaj"),
       "latitud"=>$this->input->post("latitud"),
       "longitud"=>$this->input->post("longitud")
     );
     $this->Cajero->actualizar($idcaj,$datosCajero);
     $this->session->set_flashdata("confirmacion",
     "Cajero actualizado exitosamente");
     redirect('cajeros/index');
   }
  }//Cierre de  la clase
?>
