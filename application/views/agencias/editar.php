<h1>EDITAR AGENCIAS</h1>
<form class="" method="post" action="<?php echo site_url('agencias/actualizarAgencia'); ?>">
	<input type="hidden" name="idage" id="idage"
	value="<?php echo $agenciaEditar->idage; ?>">
  <label for="">
    <b>Nombre:</b>
  </label>
  <input type="text" name="nombreage" id="nombreage"
	value="<?php echo $agenciaEditar->nombreage; ?>"
  placeholder="Ingrese el nombre..." class="form-control" required>
  <br>
  <label for="">
    <b>Gerente:</b>
  </label>
  <input type="text" name="gerenteage" id="gerenteage"
	value="<?php echo $agenciaEditar->gerenteage; ?>"
  placeholder="Ingrese el nombre..." class="form-control" required>
  <br>
  <label for="">
    <b>Ciudad:</b>
  </label>
  <input type="text" name="ciudadage" id="ciudadage"
	value="<?php echo $agenciaEditar->ciudadage; ?>"
  placeholder="Ingrese el nombre..." class="form-control" required>
  <br>
  <label for="">
    <b>Telefono:</b>
  </label>
  <input type="number" name="telefono" id="telefono"
	value="<?php echo $agenciaEditar->telefono; ?>"
  placeholder="Ingrese el nombre..." class="form-control" required>
  <br>

    <div class="row">
      <div class="col-md-6">
        <br>
        <label for="">
        <b>Latitud:</b>
      </label>
      <input type="number" name="latitud" id="latitud"
			value="<?php echo $agenciaEditar->latitud; ?>"
      placeholder="Ingrese el latitud..." class="form-control" readonly>

      </div>
      <div class="col-md-6">
        <br>
        <label for="">
        <b>Longitud:</b>
      </label>
      <input type="number" name="longitud" id="longitud"
			value="<?php echo $agenciaEditar->longitud; ?>"
      placeholder="Ingrese el longitud..." class="form-control" readonly>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div id="mapa" style="height: 250px; whidth:100%; border:1px solid black;">

      </div>
      </div>

    </div>
    <br>
    <br>
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-floppy-disk fa-bounce"></i> &nbsp Guardar</button> &nbsp &nbsp
        <a href="<?php echo site_url('agencias/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>

      </div>

    </div>
</form>

<br>
<br>
<script type="text/javaScript">
  function initMap(){
    var coordenadaCentral =
		new google.maps.LatLng(<?php echo $agenciaEditar->latitud; ?>, <?php echo $agenciaEditar->longitud; ?>);
   var miMapa= new google.maps.Map(
     document.getElementById('mapa'),{
       center: coordenadaCentral,
       zoom: 10,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     }
   );
   var marcador= new google.maps.Marker({
     position:coordenadaCentral,
     map: miMapa,
     title: 'Seleccione la ubicacion',
     draggable:true
   });
   google.maps.event.addListener(
    marcador,
    'dragend',
    function(event){
      var latitud=this.getPosition().lat();
      var longitud=this.getPosition().lng();
      document.getElementById('latitud').value=latitud;
      document.getElementById('longitud').value=longitud;
    }
   );
  }

</script>
