<!-- Agregar la siguiente estructura dentro del body de tu HTML -->

<div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="https://www.bancoamazonas.com/portals/0/banner_agencias.png" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="https://www.bancoamazonas.com/portals/0/Images/banner-web-BENEFICIOS637952451585500323.jpg?ver=2022-08-04-162558-660" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="https://www.bancoamazonas.com/portals/0/banner_CDP%20FIDELITY.png" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="https://www.bancoamazonas.com/portals/0/Images/bannerweb637569450442573550.png?ver=2021-05-18-093044-710" class="d-block w-100" alt="...">
    </div>


  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div>

<div class="row">
	<div class="col-md-6">
		<legend>Mision</legend> <hr>
		<p>
		Somos el fruto de una iniciativa empresarial privada en el Ecuador que desde 1975 ha garantizado con transparencia y solvencia, la seguridad y rentabilidad de nuestros clientes.
		</p>
	</div>
	<div class="col-md-6">
		<legend>Vison</legend> <hr>
		<p>
		Nos esmeramos en desarrollar confianza mutua entre cliente y Banco, en el contexto de una relación a largo plazo. Sabemos que es importante que el cliente se sienta cómodo con las personas y con la cultura del Banco con el que hace sus negocios.
  	</p>
	</div>

</div>
<div class="row">
	<div class="col-md-4">
		<h4>Depósito Plazo Fijo Tradicional</h4> <hr>
    <p>La obligación de pago de rendimientos es en la fecha de vencimiento, donde puede renovar el capital invertido más el interés obtenido luego de las deducciones de impuesto a la renta si aplicara; el monto mínimo de inversión es de $1.000</p>
	</div>
	<div class="col-md-4">
		<h4>Depósito Plazo Fijo Periódico</h4> <hr>
    <p>Se pacta desde el inicio con fechas periódicas de pago, permitiendo retirar los intereses acumulados de acuerdo al periodo pactado (mensual, trimestral, semestral) y su capital al vencimiento del plazo total del depósito; el CDP puede  negociarse dentro y fuera del mercado de valores; monto mínimo de inversión es de $1.000</p>
	</div>
	<div class="col-md-4">
		<h4>Depósito a Plazo Flexible</h4> <hr>
    <p>Son aquellos depósitos a corto plazo, exigibles en plazos previamente pactados, entre 8 a 29 días.
Se puede escoger el plazo según la necesidad de la inversión. Ej: 8 a 15 días.
 Monto mínimo de inversión de $10.000</p>
	</div>
  
