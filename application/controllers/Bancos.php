<?php
  class Bancos extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model("Banco");
    }

    //Renderizacion de la vista index de hospitales
    public function index(){
      $data["listadoBancos"]=$this->Banco->consultarTodos();
      $this->load->view("header");
      $this->load->view("bancos/index",$data);
      $this->load->view("footer");

    }
    //Eliminacion de hospitales recibiendo el id por get
    public function borrar($idban){
      $this->Banco->eliminar($idban);
      $this->session->set_flashdata("eliminacion","Banco eliminado exitosamente");
      redirect("bancos/index");

    }
    //Renderizacion del formulario de nuevo hospital
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("bancos/nuevo");
      $this->load->view("footer");
    }

    //Capturando datos e insertando en Hospital
    public function guardarBancos(){
      $datosNuevoBanco=array(
        "nombreban"=>$this->input->post("nombreban"),
        "paisban"=>$this->input->post("paisban"),
        "latitud"=>$this->input->post("latitud"),
        "longitud"=>$this->input->post("longitud"),
      );
      $this->Banco->insertar($datosNuevoBanco);
      $this->session->set_flashdata("confirmacion","Banco guardado exitosamente");
      enviarEmail("jose.iza9413@utc.edu.ec","CREACION",
         "<h1>SE CREO EL Banco </h1>".$datosNuevoBanco['nombreban']);
      redirect('bancos/index');
    }

    //Renderizar el formulario de edicion
    public function editar($id){
      $data["bancoEditar"]=$this->Banco->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("bancos/editar",$data);
      $this->load->view("footer");
    }
    public function actualizarBanco(){
     $idban=$this->input->post("idban");
     $datosBanco=array(
       "nombreban"=>$this->input->post("nombreban"),
       "paisban"=>$this->input->post("paisban"),
       "latitud"=>$this->input->post("latitud"),
       "longitud"=>$this->input->post("longitud")
     );
     $this->Banco->actualizar($idban,$datosBanco);
     $this->session->set_flashdata("confirmacion",
     "Banco actualizado exitosamente");
     redirect('bancos/index');
   }
  }//Cierre de  la clase
?>
