<h1 class="text-center"><i class="fa-solid fa-file-pen"></i>Reporte</h1>
<div class="row">
  <div class="col-md-12 text-end">
    <!-- Button trigger modal -->
    <center>
    <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
      <i class="fa fa-eye"></i>Ver Reporte
    </button>
  </center>

    <br> <br>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">
          <i class="fa fa-eye"></i> Mapa De Ubicaciones
        </h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div id="reporteMapa" style="height:300px;width:100%; border:2px solid blue ;">
          <!-- Aquí se mostrará el mapa -->
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-bs-dismiss="modal"><i  class="fa fa-times"></i>Cerrar</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function initMap(){
    var coordenadaCentral = new google.maps.LatLng(-1.2538824892685865, -78.62522496154902);
    var miMapa = new google.maps.Map(
      document.getElementById('reporteMapa'),
      {
        center: coordenadaCentral,
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
    );

    // Agregar marcadores para las agencias
    <?php foreach ($agencias as $agencia): ?>
      var coordenadaAgencia = new google.maps.LatLng(<?php echo $agencia->latitud_age; ?>, <?php echo $agencia->longitud_age; ?>);
      var icono = {
            url: '<?php echo base_url ('assets/img/agencias.png'); ?>', // URL de la imagen que deseas utilizar como marcador
            scaledSize: new google.maps.Size(30, 30), // Tamaño de la imagen
      };
      var marcadorAgencia = new google.maps.Marker({
        position: coordenadaAgencia,
        map: miMapa,
        title: '<?php echo $agencia->nombre_age; ?>',
        icon: icono

      });
    <?php endforeach; ?>
    <?php foreach ($cajeros as $cajero): ?>
      // Agregar marcador para cada cajero
      var coordenadaCajero = new google.maps.LatLng(<?php echo $cajero->latitud; ?>, <?php echo $cajero->longitud; ?>);
      var icono = {
            url: '<?php echo base_url ('assets/img/cajeros.png'); ?>', // URL de la imagen que deseas utilizar como marcador
            scaledSize: new google.maps.Size(30, 30), // Tamaño de la imagen
      };
      var marcadorCajero = new google.maps.Marker({
      position: coordenadaCajero,
      map: miMapa,
      title: '<?php echo $cajero->Nombre; ?>',
      icon: icono
      });
    <?php endforeach; ?>
    <?php foreach ($corresponsables as $corresponsable): ?>
      // Agregar marcador para cada corresponsable
      var coordenadaCorresponsable = new google.maps.LatLng(<?php echo $corresponsable->latitud; ?>, <?php echo $corresponsable->longitud; ?>);
      var icono = {
            url: '<?php echo base_url ('assets/img/corresponsables.png'); ?>', // URL de la imagen que deseas utilizar como marcador
            scaledSize: new google.maps.Size(30, 30), // Tamaño de la imagen
      };
      var marcadorCorresponsable = new google.maps.Marker({
      position: coordenadaCorresponsable,
      map: miMapa,
      title: '<?php echo $corresponsable->Nombre; ?>',
      icon: icono
      });
    <?php endforeach; ?>



  }
</script>
