<h1>
  <b>
    <i class="fa fa-plus-circle"></i>
    Nuevo Banco
  </b>
</h1>
<br>
<form class="" action="<?php echo site_url('bancos/guardarBancos'); ?>" method="post" enctype="multipart/form-data">
  <label for=""> <b>NOMBRE:</b> </label>
    <input type="text" name="nombreban" id="nombreban" value="" placeholder="Ingrese el nombre" class="form-control" required><br>
    <label for=""> <b>PAIS:</b> </label>
    <input type="text" name="paisban" id="paisban" value="" placeholder="Ingrese la dirección" class="form-control" requiredd><br>
    <div class="row">
        <div class="col-md-6">
          <label for=""> <b>LATITUD:</b> </label>
          <input type="number" name="latitud" id="latitud" value="" placeholder="Ingrese la latitud" class="form-control" required readonly>
        </div>
        <div class="col-md-6">
          <label for=""> <b>LONGITUD:</b> </label>
          <input type="number" name="longitud" id="longitud" value="" placeholder="Ingrese la longitud" class="form-control" required readonly>
        </div>
    </div>
  <br>
    <div class="row">
        <div class="col-md-12">
          <div id="mapa" style="height:250px; width:100%; border:1px solid black;" >

          </div>
        </div>
    </div>
  <br>
  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary"> <i class="fa fa-save fa-bounce"></i> Guardar</button> &nbsp &nbsp &nbsp &nbsp
      <a href="<?php echo site_url('bancos/index'); ?>" class="btn btn-danger"> <i class="fa fa-times fa-spin"></i> Cancelar</a>
    </div>
  </div>
</form>
<script type="text/javascript">
  function initMap(){
    var coordenadaCentral= new google.maps.LatLng(-0.9170800159461989, -78.63323527926615);
    var miMapa=new google.maps.Map(
      document.getElementById('mapa'),
      {
        center: coordenadaCentral,
        zoom: 14,
        mapTypeId:google.maps.MapTypeId.ROADMAP,
      }
    );
    var marcador = new google.maps.Marker(
      {
        position: coordenadaCentral,
        map:miMapa,
        title:'Selecciona la ubicación',
        draggable:true
      }
    );

    google.maps.event.addListener(
      marcador,
      'dragend',
      function(event){
        var latitud=this.getPosition().lat();
        var longitud=this.getPosition().lng();
        document.getElementById('latitud').value=latitud
        document.getElementById('longitud').value=longitud
      }
    );
  }
</script>


<br>
<br>
