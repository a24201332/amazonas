<?php
  class Corresponsal extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("corresponsal",$datos);
      return $respuesta;
    }
    //Consulta de datos
    function consultarTodos(){
      $corresponsales=$this->db->get("corresponsal");
      if ($corresponsales->num_rows()>0) {
        return $corresponsales->result();
      } else {
        return false;
      }
    }
    //eliminacion de hospital por id
    function eliminar($id){
        $this->db->where("idcor",$id);
        return $this->db->delete("corresponsal");
    }
    //Consulta de un solo hospital
    function obtenerPorId($id){
      $this->db->where("idcor",$id);
      $corresponsal=$this->db->get("corresponsal");
      if ($corresponsal->num_rows()>0) {
        return $corresponsal->row();
      } else {
        return false;
      }
    }
    //funcion para actualizar hospitales
    function actualizar($id,$datos){
      $this->db->where("idcor",$id);return $this->db->update("corresponsal",$datos);
    }

  }//Fin de la clase
?>
