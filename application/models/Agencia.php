<?php
  class Agencia extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("agencia",$datos);
      return $respuesta;
    }
    //Consulta de datos
    function consultarTodos(){
      $agencias=$this->db->get("agencia");
      if ($agencias->num_rows()>0) {
        return $agencias->result();
      } else {
        return false;
      }
    }
    //eliminacion de hospital por id
    function eliminar($id){
        $this->db->where("idage",$id);
        return $this->db->delete("agencia");
    }
    //Consulta de un solo hospital
    function obtenerPorId($id){
      $this->db->where("idage",$id);
      $agencia=$this->db->get("agencia");
      if ($agencia->num_rows()>0) {
        return $agencia->row();
      } else {
        return false;
      }
    }
    //funcion para actualizar hospitales
    function actualizar($id,$datos){
      $this->db->where("idage",$id);return $this->db->update("agencia",$datos);
    }

  }//Fin de la clase
?>
