<?php
  class Agencias extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model("Agencia");
    }

    //Renderizacion de la vista index de hospitales
    public function index(){
      $data["listadoAgencias"]=$this->Agencia->consultarTodos();
      $this->load->view("header");
      $this->load->view("agencias/index",$data);
      $this->load->view("footer");

    }
    //Eliminacion de hospitales recibiendo el id por get
    public function borrar($idage){
      $this->Agencia->eliminar($idage);
      $this->session->set_flashdata("eliminacion","Agencia eliminado exitosamente");
      redirect("agencias/index");

    }
    //Renderizacion del formulario de nuevo hospital
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("agencias/nuevo");
      $this->load->view("footer");
    }

    //Capturando datos e insertando en Hospital
    public function guardarAgencias(){

       /*INICIO PROCESO DE SUBIDA DE ARCHIVO*/
       $config['upload_path']=APPPATH.'../uploads/hospitales/'; //ruta de subida de archivos
       $config['allowed_types']='jpeg|jpg|png';//tipo de archivos permitidos
       $config['max_size']=5*1024;//definir el peso maximo de subida (5MB)
       $nombre_aleatorio="agencia_".time() * rand(100, 10000);//creando un nombre aleatorio
       $config['file_name']=$nombre_aleatorio;//asignando el nombre al archivo subido
       $this->load->library('upload',$config);//cargando la libreria UPLOAD
       if($this->upload->do_upload("foto")){ //intentando subir el archivo
         $dataArchivoSubido=$this->upload->data();//capturando informacion del archivo subido
         $nombre_archivo_subido=$dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
       }else{
         $nombre_archivo_subido="";//Cuando no se sube el archivo el nombre queda VACIO
       }
      $datosNuevoAgencia=array(
        "nombreage"=>$this->input->post("nombreage"),
        "gerenteage"=>$this->input->post("gerenteage"),
        "ciudadage"=>$this->input->post("ciudadage"),
        "telefono"=>$this->input->post("telefono"),
        "latitud"=>$this->input->post("latitud"),
        "longitud"=>$this->input->post("longitud"),
        "foto"=>$nombre_archivo_subido
      );
      $this->Agencia->insertar($datosNuevoAgencia);
      $this->session->set_flashdata("confirmacion","Agencia guardado exitosamente");
      enviarEmail("jose.iza9413@utc.edu.ec","CREACION",
         "<h1>SE CREO LA Agencia </h1>".$datosNuevoAgencia['nombreage']);
      redirect('agencias/index');
    }


    //Renderizar el formulario de edicion
    public function editar($id){
      $data["agenciaEditar"]=$this->Agencia->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("agencias/editar",$data);
      $this->load->view("footer");
    }
    public function actualizarAgencia(){
    $idage=$this->input->post("idage");
    $datosAgencia=array(
        "nombreage"=>$this->input->post("nombreage"),
        "gerenteage"=>$this->input->post("gerenteage"),
        "ciudadage"=>$this->input->post("ciudadage"),
        "telefono"=>$this->input->post("telefono"),
        "latitud"=>$this->input->post("latitud"),
        "longitud"=>$this->input->post("longitud")
    );
    $this->Agencia->actualizar($idage,$datosAgencia);
    $this->session->set_flashdata("confirmacion",
    "Agencia actualizado exitosamente");
    redirect('agencias/index');
}

  }//Cierre de  la clase
?>
