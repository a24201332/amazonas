<?php
  class Banco extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("banco",$datos);
      return $respuesta;
    }
    //Consulta de datos
    function consultarTodos(){
      $bancos=$this->db->get("banco");
      if ($bancos->num_rows()>0) {
        return $bancos->result();
      } else {
        return false;
      }
    }
    //eliminacion de hospital por id
    function eliminar($id){
        $this->db->where("idban",$id);
        return $this->db->delete("banco");
    }
    //Consulta de un solo hospital
    function obtenerPorId($id){
      $this->db->where("idban",$id);
      $banco=$this->db->get("banco");
      if ($banco->num_rows()>0) {
        return $banco->row();
      } else {
        return false;
      }
    }
    //funcion para actualizar hospitales
    function actualizar($id,$datos){
      $this->db->where("idban",$id);return $this->db->update("banco",$datos);
    }

  }//Fin de la clase
?>
