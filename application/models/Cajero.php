<?php
  class Cajero extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("cajero",$datos);
      return $respuesta;
    }
    //Consulta de datos
    function consultarTodos(){
      $cajeros=$this->db->get("cajero");
      if ($cajeros->num_rows()>0) {
        return $cajeros->result();
      } else {
        return false;
      }
    }
    //eliminacion de hospital por id
    function eliminar($id){
        $this->db->where("idcaj",$id);
        return $this->db->delete("cajero");
    }
    //Consulta de un solo hospital
    function obtenerPorId($id){
      $this->db->where("idcaj",$id);
      $cajero=$this->db->get("cajero");
      if ($cajero->num_rows()>0) {
        return $cajero->row();
      } else {
        return false;
      }
    }
    //funcion para actualizar hospitales
    function actualizar($id,$datos){
      $this->db->where("idcaj",$id);return $this->db->update("cajero",$datos);
    }

  }//Fin de la clase
?>
