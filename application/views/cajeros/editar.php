<h1>EDITAR BANCO</h1>
<form class="" method="post" action="<?php echo site_url('cajeros/actualizarCajero'); ?>">
	<input type="hidden" name="idcaj" id="idcaj"
	value="<?php echo $cajeroEditar->idcaj; ?>">
  <label for="">
    <b>Ciudad:</b>
  </label>
  <input type="text" name="ciudadcaj" id="ciudadcaj"
	value="<?php echo $cajeroEditar->ciudadcaj; ?>"
  placeholder="Ingrese el nombre..." class="form-control" required>
  <br>

    <div class="row">
      <div class="col-md-6">
        <br>
        <label for="">
        <b>Latitud:</b>
      </label>
      <input type="number" name="latitud" id="latitud"
			value="<?php echo $cajeroEditar->latitud; ?>"
      placeholder="Ingrese el latitud..." class="form-control" readonly>

      </div>
      <div class="col-md-6">
        <br>
        <label for="">
        <b>Longitud:</b>
      </label>
      <input type="number" name="longitud" id="longitud"
			value="<?php echo $cajeroEditar->longitud; ?>"
      placeholder="Ingrese el longitud..." class="form-control" readonly>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div id="mapa" style="height: 250px; whidth:100%; border:1px solid black;">

      </div>
      </div>

    </div>
    <br>
    <br>
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-floppy-disk fa-bounce"></i> &nbsp Guardar</button> &nbsp &nbsp
        <a href="<?php echo site_url('cajeros/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>

      </div>

    </div>
</form>

<br>
<br>
<script type="text/javaScript">
  function initMap(){
    var coordenadaCentral =
		new google.maps.LatLng(<?php echo $cajeroEditar->latitud; ?>, <?php echo $cajeroEditar->longitud; ?>);
   var miMapa= new google.maps.Map(
     document.getElementById('mapa'),{
       center: coordenadaCentral,
       zoom: 10,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     }
   );
   var marcador= new google.maps.Marker({
     position:coordenadaCentral,
     map: miMapa,
     title: 'Seleccione la ubicacion',
     draggable:true
   });
   google.maps.event.addListener(
    marcador,
    'dragend',
    function(event){
      var latitud=this.getPosition().lat();
      var longitud=this.getPosition().lng();
      document.getElementById('latitud').value=latitud;
      document.getElementById('longitud').value=longitud;
    }
   );
  }

</script>
