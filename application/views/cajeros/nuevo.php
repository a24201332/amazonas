<h1>
  <b>
    <i class="fa fa-plus-circle"></i>
    Nuevo Cajero
  </b>
</h1>
<br>
<form class="" action="<?php echo site_url('cajeros/guardarCajeros'); ?>" method="post" enctype="multipart/form-data">
  <label for=""> <b>CIUDAD:</b> </label>
    <input type="text" name="ciudadcaj" id="ciudadcaj" value="" placeholder="Ingrese el nombre" class="form-control" required><br>
        <div class="row">
        <div class="col-md-6">
          <label for=""> <b>LATITUD:</b> </label>
          <input type="number" name="latitud" id="latitud" value="" placeholder="Ingrese la latitud" class="form-control" required readonly>
        </div>
        <div class="col-md-6">
          <label for=""> <b>LONGITUD:</b> </label>
          <input type="number" name="longitud" id="longitud" value="" placeholder="Ingrese la longitud" class="form-control" required readonly>
        </div>
    </div>
  <br>
    <div class="row">
        <div class="col-md-12">
          <div id="mapa" style="height:250px; width:100%; border:1px solid black;" >

          </div>
        </div>
    </div>
  <br>
  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary"> <i class="fa fa-save fa-bounce"></i> Guardar</button> &nbsp &nbsp &nbsp &nbsp
      <a href="<?php echo site_url('cajeros/index'); ?>" class="btn btn-danger"> <i class="fa fa-times fa-spin"></i> Cancelar</a>
    </div>
  </div>
</form>
<script type="text/javascript">
  function initMap(){
    var coordenadaCentral= new google.maps.LatLng(-0.9170800159461989, -78.63323527926615);
    var miMapa=new google.maps.Map(
      document.getElementById('mapa'),
      {
        center: coordenadaCentral,
        zoom: 14,
        mapTypeId:google.maps.MapTypeId.ROADMAP,
      }
    );
    var marcador = new google.maps.Marker(
      {
        position: coordenadaCentral,
        map:miMapa,
        title:'Selecciona la ubicación',
        draggable:true
      }
    );

    google.maps.event.addListener(
      marcador,
      'dragend',
      function(event){
        var latitud=this.getPosition().lat();
        var longitud=this.getPosition().lng();
        document.getElementById('latitud').value=latitud
        document.getElementById('longitud').value=longitud
      }
    );
  }
</script>


<br>
<br>
